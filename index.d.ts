import {Framework, Utils} from "floose";
import Driver = Framework.Driver;
import Repository = Framework.Data.Repository;
import Model = Framework.Data.Model;
import {FieldInfo} from "mysql";
import SearchCriteria = Framework.Data.SearchCriteria;
import Join = Framework.Data.Join;
import Schema = Utils.Validation.Schema;

/**
 * UTILITIES
 */
export declare class MysqlUtility {
    /**
     * Convert a Date object to MySql datetime string format using UTC timezone
     * @param date
     */
    static dateToMysqlDateTime(date: Date): string;
    /**
     * Convert string format 'Y-m-d H:i:s' in UTC timezone to a valid Date object in local timezone or do nothing
     * @param mysqlDateTime
     */
    static mysqlDateTimeToDate(mysqlDateTime: any): Date;
    /**
     * Convert a Date object to MySql timestamp string format keeping local timezone
     * @param date
     */
    static dateToMysqlTimeStamp(date: Date): string;
    /**
     * Convert string format 'Y-m-d H:i:s' to a valid Date object in local timezone or do nothing
     * @param mysqlTimeStamp
     */
    static mysqlTimeStampToDate(mysqlTimeStamp: any): Date;
}

/**
 * DRIVER AND CONFIGURATION
 */
export declare interface MysqlDriverConfig {
    host: string,
    port: number,
    database: string,
    user: string,
    password: string
}

export declare class MysqlDriver extends Driver {
    readonly configurationValidationSchema: Schema;
    init<T>(config: T): Promise<void>;
    query(sql: string, values?: any): Promise<{
        results?: any;
        fields?: FieldInfo[];
    }>;
}

/**
 * MODEL AND REPOSITORY
 */
export declare abstract class AbstractMysqlModel extends Model {
}

export declare abstract class AbstractMysqlRepository<T extends AbstractMysqlModel> extends Repository<T> {
    protected _connection: string;
    constructor(connection: string);

    protected abstract _tableName(): string;
    protected abstract _tableIdentifier(): string;
    protected abstract _eventPrefix(): string;
    protected abstract _modelClass(): {new(): T};

    protected _find(searchCriteria: SearchCriteria, joinCriteria?: Join[]): Promise<T[]>;

    create(): T;
    get(identifier: any, forceReload?: boolean): Promise<T>;
    save(modelInterface: T): Promise<T>;
    delete(modelInterface: T | T[]): Promise<void>;
}